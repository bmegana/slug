﻿using UnityEngine;

public class SoundPlayer : MonoBehaviour
{
    public AudioClip stageTheme;

    private AudioSource audioSource;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    private void Start()
    {
        audioSource.PlayOneShot(stageTheme);
    }
}
