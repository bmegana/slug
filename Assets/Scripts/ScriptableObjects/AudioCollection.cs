﻿using UnityEngine;

public class AudioCollection : ScriptableObject
{
    public AudioClip stageOneTheme;
    public AudioClip stageTwoTheme;
    public AudioClip stageThreeTheme;
}
