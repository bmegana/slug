﻿using UnityEngine;

public class Slug : MonoBehaviour
{
    public float translateMult;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            transform.Translate(Vector2.right * translateMult);
        }
    }
}
