﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class SlugWithLegs : MonoBehaviour
{
    public float xSpeed;
    public float jumpMult;
    public float fallDecrement;
    public float maxFallSpeed;

    public float jumpRegisterTime;

    private Rigidbody2D rb2d;

    private bool jumpKeyPressed = false;
    private bool jumped = false;
    private bool jumpKeyReleased = false;

    private float jumpRegisterTimeInit;
    private bool jumpRegTimerStarted = false;

    private void Awake()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }

    private void Start()
    {
        rb2d.velocity = new Vector2(xSpeed, 0);

        jumpRegisterTimeInit = jumpRegisterTime;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        GameObject obj = collision.gameObject;
        if (obj.CompareTag("Ground") &&
            collision.GetContact(0).normal.y > 0)
        {
            jumpKeyPressed = false;
            jumped = false;
            jumpKeyReleased = false;
        }

        if (obj.CompareTag("Salt"))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }

    private void FixedUpdate()
    {
        rb2d.velocity = new Vector2(xSpeed, rb2d.velocity.y);

        if (jumpKeyPressed && !jumped)
        {
            jumpKeyPressed = false;
            jumped = true;
            rb2d.velocity = new Vector2(rb2d.velocity.x, jumpMult);
        }

        if (jumpRegTimerStarted && !jumped)
        {
            jumped = true;
            rb2d.velocity = new Vector2(rb2d.velocity.x, jumpMult);
        }

        if (jumped)
        {
            if (jumpKeyReleased && rb2d.velocity.y > 0)
            {
                jumpKeyReleased = false;
                rb2d.velocity = new Vector2(rb2d.velocity.x, rb2d.velocity.y / 2.0f);
            }
        }

        if (-Mathf.Abs(maxFallSpeed) < rb2d.velocity.y)
        {
            rb2d.velocity = new Vector2(rb2d.velocity.x, rb2d.velocity.y - fallDecrement);
        }

        if (rb2d.velocity.y <= -Mathf.Abs(maxFallSpeed))
        {
            rb2d.velocity = new Vector2(xSpeed, -Mathf.Abs(maxFallSpeed));
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            jumpKeyPressed = true;
            jumpKeyReleased = false;

            if (jumped)
            {
                jumpRegTimerStarted = true;
            }
        }

        if (Input.GetKeyUp(KeyCode.Space))
        {
            jumpKeyReleased = true;
            jumpKeyPressed = false;
        }

        if (jumpRegTimerStarted)
        {
            jumpRegisterTime -= Time.deltaTime;
            if (jumpRegisterTime <= 0)
            {
                jumpRegTimerStarted = false;
                jumpRegisterTime = jumpRegisterTimeInit;
            }
        }
    }
}
