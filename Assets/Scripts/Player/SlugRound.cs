﻿using UnityEngine;

public class SlugRound : MonoBehaviour
{
    private Rigidbody2D rb2d;

    public void Shoot(float zAngleDeg, float speed)
    {
        float zAngleRad = zAngleDeg * Mathf.Deg2Rad;
        Vector2 direction = new Vector2(
            Mathf.Cos(zAngleRad), Mathf.Sin(zAngleRad)
        );
        rb2d.velocity = direction * speed;
    }

    private void Awake()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }

    private void Start()
    {
        Vector3 eulerRotation = new Vector3(
            0, 0, transform.localEulerAngles.z + 30
        );
        Quaternion rotation = new Quaternion();
        rotation.eulerAngles = eulerRotation;
        transform.rotation = rotation;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (!collision.gameObject.CompareTag("Player"))
        {
            Destroy(gameObject);
        }
    }

    private void OnBecameInvisible()
    {
        Destroy(gameObject);
    }
}
