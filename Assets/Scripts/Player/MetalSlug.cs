﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MetalSlug : MonoBehaviour
{
    public float moveSpeed;

    public Transform shoulder;
    public float minShoulderZ;
    public float maxShoulderZ;
    public float aimSpeed;

    public Transform shotgun;
    public GameObject slugRoundPrefab;
    public float slugRoundSpeed;

    private Rigidbody2D rb2d;

    private float zAngleShoulder;
    private bool aimingUp;

    private void Awake()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }

    private void Start()
    {
        rb2d.velocity = new Vector2(moveSpeed, 0);

        zAngleShoulder = minShoulderZ;
        shoulder.eulerAngles = new Vector3(0, 0, zAngleShoulder);
        aimingUp = true;
    }

    private void FixedUpdate()
    {
        rb2d.velocity = new Vector2(moveSpeed, rb2d.velocity.y);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        GameObject obj = collision.gameObject;
        if (obj.CompareTag("Enemy") || obj.CompareTag("EnemyProjectile"))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }

    private void AimGun()
    {
        if (zAngleShoulder <= minShoulderZ)
        {
            aimingUp = true;
        }
        else if (zAngleShoulder >= maxShoulderZ)
        {
            aimingUp = false;
        }

        if (aimingUp)
        {
            zAngleShoulder += Time.deltaTime * aimSpeed;
        }
        else
        {
            zAngleShoulder -= Time.deltaTime * aimSpeed;
        }

        shoulder.eulerAngles = new Vector3(0, 0, zAngleShoulder);
    }

    private void Shoot()
    {
        GameObject slugRoundObj = Instantiate(
            slugRoundPrefab,
            shotgun.position,
            shoulder.rotation
        );
        SlugRound slugRound = slugRoundObj.GetComponent<SlugRound>();
        if (slugRound != null)
        {
            slugRound.Shoot(
                shoulder.localEulerAngles.z, moveSpeed + slugRoundSpeed
            );
        }
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            AimGun();
        }

        if (Input.GetKeyUp(KeyCode.Space))
        {
            Shoot();
        }
    }
}
