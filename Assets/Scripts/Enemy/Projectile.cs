﻿using UnityEngine;

public class Projectile : MonoBehaviour
{
    private Rigidbody2D rb2d;

    public void Shoot(Vector2 dir, float speed)
    {
        rb2d.velocity = dir * speed;
    }

    private void Awake()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }

    private void Start()
    {
        Vector3 eulerRotation = new Vector3(
            0, 0, transform.localEulerAngles.z + 30
        );
        Quaternion rotation = new Quaternion();
        rotation.eulerAngles = eulerRotation;
        transform.rotation = rotation;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (!collision.gameObject.CompareTag("Enemy"))
        {
            Destroy(gameObject);
        }
    }

    private void OnBecameInvisible()
    {
        Destroy(gameObject);
    }
}
