﻿using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float moveSpeed;

    public float raycastToPlayerAngleDeg;
    public float raycastDistance;

    public GameObject projectilePrefab;
    public float projectileSpeed;
    public Vector3 releasePosOffset;

    public bool repeatFire;
    public float fireRateInSeconds;

    public int health;

    private float raycastToPlayerAngleRad;
    private Vector2 raycastDir;
    private Rigidbody2D playerRb2d = null;

    private float fireRateInit;
    private bool weaponReleased = false;

    private void Start()
    {
        raycastToPlayerAngleRad = Mathf.Deg2Rad * raycastToPlayerAngleDeg;
        raycastDir = new Vector2(
            Mathf.Cos(raycastToPlayerAngleRad),
            Mathf.Sin(raycastToPlayerAngleRad)
        );

        fireRateInit = fireRateInSeconds;
    }

    private void Attack()
    {
        if (projectilePrefab != null)
        {
            Quaternion quaternion = new Quaternion
            {
                eulerAngles = new Vector3(
                    0, 0, raycastToPlayerAngleDeg
                )
            };
            GameObject projectileObj = Instantiate(
                projectilePrefab,
                transform.position + releasePosOffset,
                quaternion
            );
            Projectile proj = projectileObj.GetComponent<Projectile>();
            Vector2 targetPos = playerRb2d.position + Vector2.right * 6;
            Vector2 dir = targetPos - (Vector2)transform.position;
            proj.Shoot(dir.normalized, projectileSpeed);

            weaponReleased = true;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("SlugRound"))
        {
            health--;
            if (health <= 0)
            {
                Destroy(gameObject);
            }
        }
    }

    private void Update()
    {
        if (!weaponReleased)
        {
            RaycastHit2D hit = Physics2D.Raycast(
                transform.position, raycastDir, raycastDistance
            );

            if (hit.rigidbody != null && hit.rigidbody.CompareTag("Player"))
            {
                playerRb2d = hit.rigidbody;
            }

            if (playerRb2d != null)
            {
                Attack();
            }
        }
        else
        {
            if (repeatFire)
            {
                fireRateInSeconds -= Time.deltaTime;
                if (fireRateInSeconds <= 0)
                {
                    weaponReleased = false;
                    fireRateInSeconds = fireRateInit;
                }
            }
        }
    }
}
